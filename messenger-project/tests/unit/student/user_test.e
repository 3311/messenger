note
	description: "Summary description for {USER_TEST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	UNIT_TEST
inherit
	ES_TEST
create
	make

feature {NONE}
	make
		do
			add_boolean_case(agent t0)
	--		add_violation_case(agent t1)
	--		add_violation_case(agent t2)
	--		add_violation_case(agent t3)
			add_boolean_case(agent t4)
			add_boolean_case(agent t5)
			add_boolean_case(agent t6)
		end

feature --tests

	t0: BOOLEAN
		local
			u: USER
			m: MESSENGER
			g: GROUP
		do
			comment ("t0: a test creating a user and registering them in a group")
			create u.make (9223372036854775807, "Scott")
			create m.make
			create g.make (1, "Group")
			g.register_user (u)
			Result := u.name ~ "Scott" and u.uid = 9223372036854775807 and g.members.has (u)
		end

	t1
		local
			u:USER
		do
			comment ("t1: a test creating a user non positive ID")
			create u.make (0, "Scott")
		end

	t2
		local
			u:USER
		do
			comment ("t2: a test creating a user non alphabetic Name")
			create u.make (4, "2Scott")
		end

	t3
		local
			m: MESSENGER
		do
			comment ("t3: a test creating a user a taken ID")
			create m.make
			m.add_user (1, "Scott")
			m.add_user (1, "Farhan")
		end

	t4: BOOLEAN
		local
			u1,u2: USER
			m: MESSENGER
			me1,me2: MESSAGE
			g: GROUP
		do
			comment ("t4: a test creating two users and sending, reading and deleting messages")
			--Create users
			create u1.make (10, "Tester1")
			create u2.make (11, "Tester2")
			create m.make
			--Create group
			create g.make (1, "Group")
			g.register_user (u1)
			g.register_user (u2)
			Result := g.members.has (u1) and g.members.has (u2)
			check Result end
			--Create messages			
			create me1.make (u1, 10, g, "Test Message")
	--		g.register_message (me1)
			create me2.make (u2, 2, g, "Test2")
	--		g.register_message (me2)
			u1.read_message (me2)

			Result := g.messages.has (me1) and me1.message_status(u2) ~ "unread" and me2.message_status(u1) ~ "read"
			check Result end

			u2.read_message (me1)
			Result := me1.message_status(u2) ~ "read"
			check Result end

			u2.delete_message (me1)
			Result := me1.message_status(u2) ~ "unavailable"
		end

	t5: BOOLEAN
		local
			u1,u2: USER
			m: MESSENGER
			me1,me2,me3,me4: MESSAGE
			g: GROUP
			test_string: STRING
		do
			comment ("t5: a test creating two users and sending, listing new/old messages")
			--Create users
			create u1.make (10, "Tester1")
			create u2.make (11, "Tester2")
			create m.make
			--Create group
			create g.make (1, "Group")
			u1.enter_group (g)
			u2.enter_group (g)
			--Create messages
			create me1.make (u1, 1, g, "Hello.")
			create me2.make (u1, 2, g, "Are you there?")
			create me3.make (u1, 3, g, "???")
			create me4.make (u2, 4, g, "Hi. I'm just here to test a very long message and see how it outputs.")

			create test_string.make_empty
			test_string.append (me1.out + me2.out + me3.out)

			--comment(g.messages.count.out)
			Result := test_string ~ u2.list_new_messages
			check Result end

			u2.read_message (me1)
			test_string.make_empty
			test_string.append (me2.out + me3.out)
			Result := test_string ~ u2.list_new_messages
			check Result end

			create test_string.make_empty
			test_string.append (me1.out + me2.out + me3.out + me4.out)
			u1.read_message (me4)
			Result := u1.list_old_messages ~ test_string
			check Result end

			u1.delete_message (me2)
			create test_string.make_empty
			test_string.append (me1.out + me3.out + me4.out)
			sub_comment("Expected: " +test_string + "%N" + "Actual: " + u1.list_old_messages)
			Result := u1.list_old_messages ~ test_string
			check Result end
		end

	t6: BOOLEAN
		local
			u1,u2: USER
			m: MESSENGER
			me1,me2: MESSAGE
			g,g2: GROUP
		do
			comment ("t6: a test creating two users and sending messages in different groups")
			--Create users
			create u1.make (10, "Tester1")
			create u2.make (11, "Tester2")
			create m.make
			--Create group
			create g.make (1, "Group")
			create g2.make (2, "Group2")
			u1.enter_group (g)
			u2.enter_group (g)
			u1.enter_group (g2)

			create me1.make (u1, 1, g2, "You cant see this.")
			create me2.make (u1, 2, g, "But you can see this.")
			Result := u2.list_new_messages ~ me2.out
		end
end
