note
	description: "Singleton access to the default business model."
	author: "Jackie Wang"
	date: "$Date$"
	revision: "$Revision$"

expanded class
	ETF_MODEL_ACCESS

feature
	m: MESSENGER
		once
			create Result.make
		end
	p: PREVIEWABLE
		once
			create Result.maker
		end

invariant
	m = m
	p = p
end




