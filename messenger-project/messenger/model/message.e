note
	description: "Summary description for {MESSAGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MESSAGE
inherit
	ANY
	undefine
		is_equal
	redefine
		out
	end
	REPORTABLE
	undefine
		is_equal,
		out
	end
	COMPARABLE
	undefine
		out
	end
	PREVIEWABLE
	undefine
		out,
		is_equal
	end

create
	make

feature {NONE}
	make(send: USER;id: INTEGER_64; a_group:GROUP; text: STRING; length: INTEGER_64)
		require
			positive_id:
				id > 0
			message_not_empty:
				not text.is_empty
			has_sender:
				send /= Void
			has_group:
				a_group /= Void
		do
			--Initialize attributes
			body := text
			MID := id
			sender := send
			group := a_group
			set_message_preview(length)

			--Send message to mailbx of everyone in group
			send_to_others

			--Put into appropriate group
			a_group.register_message (current)
		ensure
			not_empty:
				not body.is_empty
			has_id:
				MID = id
			has_group:
				group /= Void
		end

	--Set status of all members in group to not have read message yet
	send_to_others
		require
			has_sender:
				sender /= Void
		do
			create mstatus.make (0)
			--Loop over all members in the group recieving message
			across
				group.members as gm
			loop
				--Set it such that nobody has read message yet
				mstatus.force ("unread", gm.item.uid)
			end

			--Overwrite value for sender, sender must have read message
			mstatus[sender.uid] := "read"

		ensure

		end

feature --attributes
	MID: INTEGER_64  --ID of message
	body: STRING  --Text message contains
	sender: USER  --Sender of the message
	group: GROUP  --Group message was sent in


feature {NONE} --status
	--User ID -> Status for that user
	mstatus: HASH_TABLE[STRING, INTEGER_64]

	--Alter status of message for a given user
	set_mstatus(u: USER; s: STRING)
		require
			user_has_message:
				not message_status(u).is_empty
			not_unavailable:
				message_status(u) /~ s_unavailable
		do
			--Alter status of message for user
			mstatus.at (u.uid) := s
		ensure
			correct_status:
				mstatus.at (u.uid) ~ s
		end

	--Ensure that all users in group have recieved message and message is unread for all but sender
	sent_checker: BOOLEAN
		do
			Result := TRUE

			across
				group.members as m
			loop
				--Sender must have read message
				if m.item = sender then
					Result := Result and message_status(m.item) ~ s_read
				else
					--Nobody else has yet read message
					Result := message_status(m.item) ~ s_unread
				end
			end
		ensure
			nothing_chnged:
				mstatus ~ old mstatus
		end

feature --commands
	--User reads message
	read(u:USER)
		require
			user_in_message_group:
				u.groups.has (group)
			message_unread:
				message_status(u) ~ "unread"
		do
			--Set message status to read
			set_mstatus(u, s_read)
		ensure
			message_read:
				mstatus.at (u.uid) ~ "read"
		end

	--User deletes message
	delete(u: USER)
		require
			user_in_message_group:
				u.groups.has (group)
			message_read:
				message_status (u) ~ "read"
		do
			--Set status to unavailable
			set_mstatus(u, s_unavailable)
		ensure
			message_deleted:
				mstatus.at (u.uid) ~ "unavailable"
		end


feature --queries
	--Print all message details
	out : STRING

		do
			create Result.make_from_string ("  ")
			if body.count > p_length then
				--Truncate message
				Result.append ("    " + MID.out + "->[sender: " + sender.UID.out + ", group: " + group.gid.out + ", content: %"" + body.head (p_length.as_integer_32) + "...%"]")
			else
				Result.append ("    " + MID.out + "->[sender: " + sender.UID.out + ", group: " + group.gid.out + ", content: %"" + body + "%"]")
			end
		ensure then
			not_empty:
				Result /~ "  "
		end

	--Print out the message status for all users
	message_state: STRING
		do
			create Result.make_empty
			--Iterate over all members that can see this message
			across
				group.sort_users as g
			loop
				--Attach state for that user to end of message
				if attached mstatus[g.item.uid] as s then
					Result.append ("%N      (" + g.item.uid.out + ", " + mid.out + ")->" + s)
				end
			end
		end

	--Find message status for a given user (Read, unread, unavailable)
	message_status(u: USER): STRING
		do
			create Result.make_empty
			if attached mstatus.at (u.uid) as a then
				Result.append (a)
			else
				Result.append ("unavailable")
			end
		end

	is_less alias "<" (other:MESSAGE): BOOLEAN
		do
			Result := FALSE
			if other.mid  > mid then
				Result := TRUE
			end
		end



feature {NONE} --invariant helpers
	--Everyone has valid status
	status_check: BOOLEAN
		do
			across
				mstatus as s
			loop
				Result := not message_state.is_empty
			end
		end

invariant
	all_have_valid_status:
		status_check

end
