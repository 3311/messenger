note
	description: "A default business model."
	author: "Jackie Wang"
	date: "$Date$"
	revision: "$Revision$"

class
	MESSENGER

inherit
	ANY
		redefine
			out
		end
	REPORTABLE
		undefine
			out
		end

create --{ETF_MODEL_ACCESS}
	make

feature {NONE} -- Initialization
	make
			-- Initialization for `Current'.
		do
			create s.make_empty
			i := 0
			create users.make
			create groups.make

			set_status(s_ok)
			set_report(r_skip)
			p_length := 15
		ensure
			p_length = 15
		end

feature -- model attributes
	s : STRING
	i : INTEGER
	users: SORTED_TWO_WAY_LIST[USER]
	groups: SORTED_TWO_WAY_LIST[GROUP]
	p_length: INTEGER_64

feature --Adding commands
	--Create and add new user to system
	add_user(id: INTEGER_64; a_name: STRING)
		require
			id_not_taken:
				not user_present(id)
			name_alphabetic:
				a_name.at(1).is_alpha
			id_positive:
				id > 0
		local
			new: USER
		do
			create new.make (id, a_name)
			--Place them in users
			users.force (new)
		ensure
			consistent_count:
				users.count = old users.count + 1
			user_present:
				user_present(id)
		end

	--Create and add new group to system
	add_group(gid: INTEGER_64; a_name: STRING)
		require
			id_positive:
				gid > 0
			id_not_taken:
				not group_exists(gid)
			name_alphabetic:
				a_name.at(1).is_alpha
		local
			new: GROUP
		do
			create new.make (gid, a_name)
			--Register new group
			groups.force(new)
		ensure
			consistent_count:
				groups.count = old groups.count + 1
			group_present:
				group_exists(gid)
		end

	set_message_preview(length: INTEGER_64)
		require
			positive_length:
				length >= 0
		do

			p_length := (length)
			across
				groups as g
			loop
				across
					g.item.messages as m
				loop
					m.item.set_message_preview (length)
				end
			end
		ensure
			p_length = length
		end

feature -- queries
	get_length: INTEGER_64
		do
			Result := p_length
		end
	list_groups: STRING
	local
		sorted_names: SORTED_TWO_WAY_LIST[STRING]
		do
			create Result.make_empty
			create sorted_names.make
			across
				groups as g
			loop
				sorted_names.force (g.item.name)
			end

			across
				sorted_names as sn
			loop
				if attached get_group_by_name(sn.item) as found then
					if sn.cursor_index /= sn.first_index then
						Result.append("%N")
					end
					Result.append("  " + found.out)
				end
			end
		end

	get_group_by_name(name: STRING): detachable GROUP
		do
			across
				groups as g
			loop
				if g.item.name ~ name then
					Result := g.item
				end
			end
		end

	list_users: STRING
	local
		sorted_names: SORTED_TWO_WAY_LIST[STRING]
		do
			create Result.make_empty
			create sorted_names.make
			across
				users as u
			loop
				sorted_names.force (u.item.name)
			end

			across
				sorted_names as sn
			loop
				if attached get_user_by_name(sn.item) as found then
					if sn.cursor_index /= sn.first_index then
						Result.append("%N")
					end
					Result.append("  " + found.out)
				end
			end

		end

	get_user_by_name(name: STRING): detachable USER
		do
			across
				users as u
			loop
				if u.item.name ~ name then
					Result := u.item
				end
			end
		end


	group_exists(id: INTEGER_64): BOOLEAN
		do
			Result := FALSE
			across
				groups as g
			loop
				Result := Result or g.item.gid = id
			end
		end

	user_present(id: INTEGER_64): BOOLEAN
		do
			Result := FALSE
			across
				users as u
			loop
				Result := Result or u.item.uid = id
			end
		end

	--Find group using group ID
	get_group_by_id(id: INTEGER_64): detachable GROUP
		require
			group_exists:
				group_exists(id)
		do
			--Search through all groups for correct one
			across
				groups as g
			loop
				if g.item.gid = id then
					Result := g.item
				end
			end
		ensure
--			correct_group_found:
--				Result.gid = id
		end

	--Find user using user ID
	get_user_by_id(id: INTEGER_64): detachable USER
		require
			user_exists:
				user_present(id)
		do
			across
				users as u
			loop
				if u.item.uid = id then
					Result := u.item
				end
			end

		ensure
--			correct_user_found:
--				Result.uid implies Result.uid = id
		end

	--Count number of messages in system
	message_count: INTEGER
		do
			Result := 0
			across
				groups as g
			loop
				across
					g.item.messages as m
				loop
					Result := Result + 1
				end
			end
		end

	out : STRING
		do
			create Result.make_from_string ("  ")
			Result.append (i.out + ":  " + status + r_message)
			if report = r_default then
				Result.append ("%N" + default_out)
			elseif report /= r_skip then
				Result.append ("%N")
				if report /~ list_groups and report /~ list_users then
					Result.append ("  ")
				end
				  Result.append (report)
			end
			set_message("")
			Result.append ("%N")
		end

feature {NONE}
	default_out: STRING
		do
			create Result.make_empty
			Result.append("  Users:%N")
			across
				users as u
			loop
				Result.append("  " + four_spaces(report = r_default) + u.item.out + "%N")
			end
			Result.append ("  Groups:%N")
			across
				groups as g
			loop
				Result.append("  " + four_spaces(report = r_default) + g.item.out + "%N")
			end
			Result.append ("  Registrations:%N")
			across
				users as u
			loop
				Result.append(u.item.list_groups)
			end
			Result.append ("  All messages:%N")
			Result.append (print_messages)
			Result.append ("  Message state:")
			Result.append (print_states)

			--list registrations, all messages
		end

	print_states: STRING
		local
			m_list: SORTED_TWO_WAY_LIST[MESSAGE]
		do
			create m_list.make
			create Result.make_empty
			--Collect all messages
			across
				groups as g
			loop
				across
					g.item.messages as m
				loop
					--Ensure message hasnt already been collected
					if not m_list.has(m.item) then
						m_list.force(m.item)
					end
				end
			end

			--Check status for all users and all messages
			across
				m_list as m
			loop
				across
					users as u
				loop
					--Check that the user is in a group (can recieve messages)
					if not u.item.groups.is_empty then
						Result.append("%N      (" + u.item.uid.out + ", " + m.item.mid.out + ")->" + m.item.message_status (u.item))
					end
				end
			end
		end

	--Sort and print out messages
	print_messages: STRING
		local
			m_list: SORTED_TWO_WAY_LIST[MESSAGE]
		do
			create Result.make_empty
			create m_list.make

			--Collect all messages to sort them
			across
				groups as g
			loop
				across
					g.item.messages as m
				loop
					if not m_list.has (m.item) then
						m_list.force (m.item)
					end
				end
			end

			--Print out the now sorted list of messages
			across
				m_list as m
			loop
				Result.append(m.item.out + "%N")
			end

		end


feature -- model operations
	default_update
			-- Perform update to the model state.
		do
			i := i + 1
			--Remove message, report, error status
	--		remove_report
	--		set_message("")
		end

	reset
			-- Reset model state.
		do
			make
		end
end




