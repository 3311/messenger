note
	description: "Summary description for {USER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	USER
inherit
	ANY
	undefine
		is_equal
	redefine
		out
	end
	COMPARABLE
	undefine
		out
	end

create
	make

feature {NONE}
	make(id: INTEGER_64;a_name:STRING)
		require
			positive_id:
				id > 0
			alphaabetic_name:
				a_name.at (1).is_alpha
		do
			uid	 := id
			name := a_name
			create groups.make
		ensure
			not_member_of_any_group:
				groups.is_empty
		end

feature --attributes
	uid: INTEGER_64  --Unique user ID
	name: STRING  --User name
	groups: SORTED_TWO_WAY_LIST[GROUP]  --Groups user is registered in


feature --commands
	--User joins group
	enter_group(g: GROUP)
		require
			not_in_group:
				not groups.has (g)
		do
			--Add user to group, record group in list of groups
			g.register_user (current)
			groups.force (g)
		ensure
			in_group:
				groups.has (g)
		end


feature --queries
	list_new_messages: STRING
		require
			in_group: --Must be in a group to recieve messages
				not groups.is_empty
		do
			create Result.make_empty

			--Iterate over all message this user has
			across
				get_messages as m
			loop
				--Check if message is unread
				if(m.item.message_status(current) ~ "unread") then
					Result.append ("%N"+m.item.out)
				end
			end

			--If Result is empty this user has no new messages
			if Result.is_empty then
				Result.append("There are no new messages for this user.")
			else
				--Otherwise stick message at top of report
				Result.prepend ("New/unread messages for user [" + uid.out + ", " + name + "]:")
			end
		end

	list_old_messages: STRING
		require
			in_group: --Must be in a group to recieve messages
				not groups.is_empty
		do
			create Result.make_empty

			--Iterate over all message this user has
			across
				get_messages as m
			loop
				--Check if message is read
				if(m.item.message_status(current) ~ "read") then
					Result.append ("%N"+m.item.out)
				end
			end

			--If Result is empty this user has no old messages
			if Result.is_empty then
				Result.append("There are no old messages for this user.")
			else
				Result.prepend ("Old/read messages for user [" + uid.out + ", " + name + "]:")
			end
		end

	list_groups: STRING
		do
			create Result.make_empty
			across
				groups as g
			loop
				--If this isnt the first group in the list then precede with comma (formatting)
				if not Result.is_empty then
					Result.append(", ")
				end
				--Append all groups user is present in
				Result.append (g.item.gid.out + "->" + g.item.name)
			end

			--If the user is in groups then fix formatting
			if not Result.is_empty then
				Result.prepend ("      [" + uid.out + ", " + name + "]->{")
				Result.append ("}%N")
			end
		end

	is_less alias "<" (other:USER): BOOLEAN
		do
			Result := FALSE
			if other.uid  > uid then
				Result := TRUE
			end
		end

feature --messages
	read_message(mess: MESSAGE)
		require
			in_correct_group:
				groups.has (mess.group)
			meesage_unread:
				mess.message_status (current) ~ "unread"
		do
			mess.read (current)
		end

	delete_message(message: MESSAGE)
		require
			in_correct_group:
				groups.has (message.group)
			meesage_read_and_not_deleted:
				message.message_status (current) ~ "read"
		do
			message.delete(current)
		ensure
			message_no_longer_available:
				message.message_status (current) ~ "unavailable"
		end

feature --queries
	out: STRING
		do
			create Result.make_empty
			Result.append (uid.out + "->" + name.out)
		end

feature  {ETF_COMMAND} --helper queries

	--Get all messages from all groups user is member of
	get_messages: ARRAY[MESSAGE]
		require
			in_group: --Must be in a group to recieve messages
				not groups.is_empty
		local
			inner: ARRAY[MESSAGE]
		do
			create Result.make_empty

			--Iterate over all groups this user is in
			across
				groups as g
			loop
				--Collect all messages from users groups
				create inner.make_from_array (g.item.list_messages)
				across
					inner as i
				loop
					--Collect messages from group
					Result.force (i.item, Result.count)
				end
			end
		end

		get_message_by_id(mid: INTEGER_64): detachable MESSAGE
			do
				across
					get_messages as m
				loop
					if m.item.mid = mid then
						Result := m.item
					end
				end
			end
end
