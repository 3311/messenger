note
	description: "Summary description for {MESSAG}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PREVIEWABLE
create
	maker

feature {NONE}
	maker
	once
		p_length := 15
	end

feature
	p_length: INTEGER_64

feature
	--Chang the preview length
	set_message_preview(length: INTEGER_64)
		require
			positive_length:
				length >= 0
		do
			p_length := (length)
		ensure
			p_length = length
		end

	get_length: INTEGER_64
		do
			Result := p_length
		end


end
