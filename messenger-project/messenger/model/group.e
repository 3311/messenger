note
	description: "Summary description for {GROUP}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GROUP
inherit
	ANY
	undefine
		is_equal
	redefine
		out
	end
	COMPARABLE
	undefine
		out
	end

create
	make

feature {NONE}
	make(id: INTEGER_64; a_name: STRING)
		require
			positive_id:
				id > 0
			name_is_alphabetic:
				a_name.at (1).is_alpha
		do
			--Assign attributes
			GID := id
			name := a_name
			create messages.make_empty
			create members.make_empty
		ensure
			has_name:
				name ~ a_name
			has_id:
				GID ~ id
			no_messages:
				messages.is_empty
			no_members:
				members.is_empty
		end

feature --attributes
	GID: INTEGER_64  --Group ID
	name: STRING  --Name of group
	members: ARRAY[USER]  --All members in group

feature --Security sensitive attributes
	messages: ARRAY[MESSAGE]  --All messages posted in group

feature --commands
	--Put new user into group
	register_user(new: USER)
		require
			user_not_already_in_group:
				not members.has (new)
		do
			--Add user to group
			members.force (new, members.count + 1)
		ensure
			member_present:
				members.has (new)
			one_more_user:
				members.count = old members.count + 1
			member_has_no_messages:
				new_user_message_check(new)
		end

	--Place message in chat log
	register_message(new: MESSAGE)
		require
			doesnt_exist_yet:
				not messages.has (new)
		do
			--Post message in group
			messages.force (new, messages.count + 1)
		ensure
			have_message:
				messages.has (new)
			one_more_message:
				messages.count = old messages.count + 1
		end

feature --queries
	--Used in ordering list of groups by ID
	is_less alias "<" (other:GROUP): BOOLEAN
		do
			Result := FALSE
			if other.gid > gid then
				Result := TRUE
			end
		end

	--Get all messages
	list_messages: ARRAY[MESSAGE]
		do
			create Result.make_empty
			across
				messages as m
			loop
				Result.force(m.item, Result.count + 1)
			end
		end


	--Sort users for printing out
	sort_users: SORTED_TWO_WAY_LIST[USER]
		do
			--Make sorted two way list handle the sorting
			create Result.make
			across
				members as m
			loop
				Result.force (m.item)
			end
		end

	--Print out group ID and name
	out: STRING
		do
			create Result.make_empty
			Result.append (gid.out + "->" + name.out)
		end


feature {NONE} --Postcondition/invariant helpers
	--Ensure a new user in group cant read previously sent messages
	new_user_message_check(u: USER): BOOLEAN
		do
			Result := TRUE
			across
				messages as m
			loop
				Result := Result and m.item.message_status (u) ~ "unread"
			end
		end
end
