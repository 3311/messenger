note
	description: "Summary description for {REPORTABLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

expanded class
	REPORTABLE

feature {ETF_COMMAND, MESSENGER}
	report: STRING
		attribute create Result.make_empty end
	status: STRING
		attribute create Result.make_empty end
	s_ok: STRING = "OK"
	s_error: STRING = "ERROR "
	r_default: STRING = "d"
	r_non_positive_id: STRING = "ID must be a positive integer."
	r_name_non_alpha: STRING = "User name must start with a letter."
	r_group_non_alpha: STRING = "Group name must start with a letter."
	r_no_groups: STRING = "There are no groups registered in the system yet."
	r_id_taken: STRING = "ID already in use."
	r_not_in_group_post: STRING = "User not authorized to send messages to the specified group."
	r_no_user: STRING = "User with this ID does not exist."
	r_empty_message: STRING = "A message may not be an empty string."
	r_no_users: STRING = "There are no users registered in the system yet."
	r_no_group: STRING = "Group with this ID does not exist."
	r_no_message: STRING = "Message with this ID does not exist."
	r_message_not_found: STRING = "Message with this ID not found in old/read messages."
	r_message_alread_read: STRING = "Message has already been read. See `list_old_messages'."
	r_no_new_messages: STRING = "There are no new messages for this user."
	r_non_postitive_preview: STRING = "Message length must be greater than zero."
	r_already_registered: STRING = "This registration already exists."
	r_skip: STRING = ""
	r_message: STRING
		attribute create Result.make_empty end


	set_message(text: STRING)
		do
			create r_message.make_from_string (text)
		end

	four_spaces(test: BOOLEAN): STRING
		do
			create Result.make_empty
			if test then
				create Result.make_from_string("    ")
			end
		end

	set_status(new_status: STRING)
		do
			status := new_status
		end
	set_report(new_report: STRING)
		do
			report := new_report
		end
	remove_report
		do
			report := ""
		end


	feature{MESSAGE} --Status attributes for message
	s_read: STRING = "read"
	s_unread: STRING = "unread"
	s_unavailable: STRING = "unavailable"
end
