note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_LIST_USERS
inherit
	ETF_LIST_USERS_INTERFACE
		redefine list_users end
create
	make
feature -- command
	list_users
    	do
    		model.set_status(model.s_ok)
    		model.remove_report
    		--Check if users exist
			if model.users.count < 1 then
				model.set_report (model.r_no_users)
			else
				model.set_report(model.list_users)
			end

			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
