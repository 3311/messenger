note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_ADD_USER
inherit
	ETF_ADD_USER_INTERFACE
		redefine add_user end
create
	make
feature -- command
	add_user(uid: INTEGER_64 ; user_name: STRING)
		require else
			add_user_precond(uid, user_name)
    	do
    		model.set_status (model.s_error)
    		model.remove_report
    		--Check uid is positive
    		if uid < 1 then
    			model.set_report(model.r_non_positive_id)
    		--Check if ID is taken already
    		elseif model.user_present (uid) then
				model.set_report (model.r_id_taken)
    		--Check name is correct format
    		elseif user_name.is_empty or not user_name.at(1).is_alpha then
				model.set_report(model.r_name_non_alpha)
			--Everything is good
			else
				model.set_status (model.s_ok)
				model.set_report (model.r_default)
				model.add_user(uid, user_name)
    		end

			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
