note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_LIST_GROUPS
inherit
	ETF_LIST_GROUPS_INTERFACE
		redefine list_groups end
create
	make
feature -- command
	list_groups
    	do
    		model.remove_report
    		model.set_status (model.s_ok)
    		--Check if any groups exist
    		if model.groups.count < 1 then
    			model.set_report (model.r_no_groups)
    		--All good
    		else
    			model.set_report (model.list_groups)
    		end
			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
