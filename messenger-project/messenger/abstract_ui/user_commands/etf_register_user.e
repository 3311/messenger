note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_REGISTER_USER
inherit
	ETF_REGISTER_USER_INTERFACE
		redefine register_user end
create
	make
feature -- command
	register_user(uid: INTEGER_64 ; gid: INTEGER_64)
		require else
			register_user_precond(uid, gid)
    	do
    		model.set_status (model.s_error)
    		model.remove_report
    		--Check ID is positive
    		if uid < 1 or gid < 1 then
				model.set_report (model.r_non_positive_id)
    		--Check user exists
    		elseif not model.user_present (uid) then
				model.set_report (model.r_no_user)
    		--Check group exists
    		elseif not model.group_exists (gid) then
				model.set_report (model.r_no_group)
			--Check user is not already in group
			elseif (attached model.get_group_by_id (gid) as g and attached model.get_user_by_id (uid) as m) and then g.members.has (m) then
				model.set_report (model.r_already_registered)
    		else
    			if attached model.get_user_by_id(uid) as user and attached model.get_group_by_id (gid) as group then
					model.set_status (model.s_ok)
					model.set_report (model.r_default)

					user.enter_group (group)
				end

    		end
			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
