note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_SEND_MESSAGE
inherit
	ETF_SEND_MESSAGE_INTERFACE
		redefine send_message end
create
	make
feature -- command
	send_message(uid: INTEGER_64 ; gid: INTEGER_64 ; txt: STRING)
		require else
			send_message_precond(uid, gid, txt)
		local
			m: MESSAGE
    	do
    		model.set_status (model.s_error)
    		model.remove_report
    		--Check IDs are positive
    		if uid < 1 or gid < 1 then
    			model.set_report(model.r_non_positive_id)
    		--Check user exists
    		elseif not model.user_present (uid) then
				model.set_report (model.r_no_user)
			--Check group exists
			elseif not model.group_exists (gid) then
				model.set_report (model.r_no_group)
			--Check that user is in group
			elseif not ((attached model.get_group_by_id (gid) as g and attached model.get_user_by_id (uid) as u) and then g.members.has (u)) then
				model.set_report (model.r_not_in_group_post)
			--Check if message is empty
			elseif txt.is_empty then
				model.set_report (model.r_empty_message)
			--All good	
			else
				if attached model.get_user_by_id (uid) as user and attached model.get_group_by_id (gid) as group then
					create m.make (user, model.message_count+1, group, txt, preview.get_length)
				end
				model.set_status (model.s_ok)
				model.set_report (model.r_default)
    		end
			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
