note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_READ_MESSAGE
inherit
	ETF_READ_MESSAGE_INTERFACE
		redefine read_message end
create
	make
feature -- command
	read_message(uid: INTEGER_64 ; mid: INTEGER_64)
		require else
			read_message_precond(uid, mid)
    	do
    		model.set_status (model.s_error)
    		model.remove_report
    		--Check ID is positive
    		if uid < 1then
				model.set_report (model.r_non_positive_id)
    		--Check user exists
    		elseif not model.user_present (uid) then
				model.set_report (model.r_no_user)
			--Check message exists
			elseif model.message_count < mid then
				model.set_report (model.r_no_message)
			--All good
			else
				if attached model.get_user_by_id (uid) as user and then attached user.get_message_by_id (mid) as message then
					--Check user hasnt already deleted message and hasnt read message
					if message.message_status(user) /~ "unread" then
						model.set_report (model.r_message_alread_read)
		--			elseif message.message_status (user) ~ "unavailable" then
		--				model.set_report (model.r_message_not_found)
					else
						model.set_status (model.s_ok)
						model.set_report (model.r_default)
						user.read_message(message)

						model.set_message("%N  Message for user " + "[" + user.uid.out + ", " + user.name + "]: " + "[" + message.mid.out + ", %"" + message.body + "%"]")
					end
				end


			end
			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
