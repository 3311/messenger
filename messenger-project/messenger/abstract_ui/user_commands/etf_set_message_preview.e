note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_SET_MESSAGE_PREVIEW
inherit
	ETF_SET_MESSAGE_PREVIEW_INTERFACE
		redefine set_message_preview end
create
	make
feature -- command
	set_message_preview(n: INTEGER_64)
    	do
    		model.remove_report
    		--Check size >0
    		if n < 1 then
    			model.set_status (model.s_error)
    			model.set_report (model.r_non_postitive_preview)
    		else
    			across
    				model.groups as g
    			loop
    				across
    					g.item.messages as m
    				loop
						m.item.set_message_preview(n)
    				end
    			end
    			model.set_status (model.s_ok)
    			model.set_report (model.r_default)
    		end
			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
