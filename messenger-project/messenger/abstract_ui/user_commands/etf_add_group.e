note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_ADD_GROUP
inherit
	ETF_ADD_GROUP_INTERFACE
		redefine add_group end
create
	make
feature -- command
	add_group(gid: INTEGER_64 ; group_name: STRING)
		require else
			add_group_precond(gid, group_name)
    	do
    		model.set_status (model.s_error)
    		model.remove_report
    		--Check gid is positive
    		if gid < 1 then
    			model.set_report(model.r_non_positive_id)
    		--Check if ID is taken already
    		elseif model.group_exists (gid) then
				model.set_report (model.r_id_taken)
    		--Check name is correct format
    		elseif group_name.is_empty or not group_name.at(1).is_alpha then
				model.set_report(model.r_group_non_alpha)
			--Everything is good
			else
				model.set_status (model.s_ok)
				model.set_report (model.r_default)
				model.add_group(gid, group_name)
    		end

			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
