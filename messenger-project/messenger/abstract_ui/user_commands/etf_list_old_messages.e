note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_LIST_OLD_MESSAGES
inherit
	ETF_LIST_OLD_MESSAGES_INTERFACE
		redefine list_old_messages end
create
	make
feature -- command
	list_old_messages(uid: INTEGER_64)
		require else
			list_old_messages_precond(uid)
    	do
    		model.set_status (model.s_error)
    		model.remove_report
    		--Check ID is positive
    		if uid < 1then
				model.set_report (model.r_non_positive_id)
    		--Check user exists
    		elseif not model.user_present (uid) then
				model.set_report (model.r_no_user)
			else
				if attached model.get_user_by_id (uid) as user then
					model.set_status (model.s_ok)
					model.set_report (user.list_old_messages)
				end
			end
			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
