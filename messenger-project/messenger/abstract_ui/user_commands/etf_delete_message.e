note
	description: ""
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ETF_DELETE_MESSAGE
inherit
	ETF_DELETE_MESSAGE_INTERFACE
		redefine delete_message end
create
	make
feature -- command
	delete_message(uid: INTEGER_64 ; mid: INTEGER_64)
		require else
			delete_message_precond(uid, mid)
    	do
    		model.set_status (model.s_error)
    		model.remove_report
    		--Check ID is positive
    		if uid < 1then
				model.set_report (model.r_non_positive_id)
    		--Check user exists
    		elseif not model.user_present (uid) then
				model.set_report (model.r_no_user)
			--Check message exists
			elseif model.message_count < mid then
				model.set_report (model.r_no_message)
			--All good
			else
				if attached model.get_user_by_id (uid) as user and then attached user.get_message_by_id (mid) as message then
					--Check user hasnt already deleted message and has read message
					if message.message_status(user) /~ "read" then
						model.set_report (model.r_message_not_found)
					else
						model.set_status (model.s_ok)
						model.set_report (model.r_default)
						user.delete_message(message)
					end
				else
					--Message not found for this user
					model.set_report (model.r_message_not_found)
				end


			end
			-- perform some update on the model state
			model.default_update
			etf_cmd_container.on_change.notify ([Current])
    	end

end
